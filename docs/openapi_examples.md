```
private function openapiArr(): array
{
    //'paths' => [
    return [
        '/article/{slug}/part/{part}?{exten}' => [
            '_resource' => __CLASS__,
            'get' => [
                'summary' => 'Single article',
                'description' => 'Single article in JSON format',
                'parameters' => [
                    [
                        'name' => 'slug',
                        'in' => 'path',
                        'required' => true,
                        'schema' => [
                            'type' => 'string',
                            'enum' => ['json', 'xml'],
                            'default' => 'json',
                        ],
                    ],
                    [
                        'name' => 'part',
                        'in' => 'path',
                        'required' => true,
                        'content' => [
                            'application/json' => [  # <---- media type indicates how to serialize / deserialize the parameter content
                                'schema' => [
                                    'type' => 'object',
                                    'properties' => [
                                        'tags' => [
                                            'type' => 'array',
                                            'items' => [
                                                'type' => 'object',
                                                'properties' => [
                                                    'id' => [
                                                        'type' => 'integer',
                                                        'format' => 'int64',
                                                    ],
                                                    'tagName' => [
                                                        'type' => 'string',
                                                    ],
                                                ],
                                            ],
                                        ],
                                        'color' => [
                                            'type' => 'string',
                                        ],
                                    ],

                                ],
                            ],
                        ],
                    ],
                    [
                        'name' => 'exten',
                        'in' => 'query',
                        'required' => false,
                        'schema' => [
                            'type' => 'array',
                            'items' => [
                                'type' => 'integer',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ];
}
```
