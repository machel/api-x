<?php

declare(strict_types=1);

namespace ApiX\Type;

enum NumberFormat: string implements FormatInterface
{
    case DEFAULT = '-';
    case FLOAT = 'float';
    case DOUBLE = 'double';
}
