<?php

declare(strict_types=1);

namespace ApiX\Type;

class OasBoolean extends OasType
{
    public function value(array $requestParamData): bool
    {
        // TODO - better casting (check for 1/0, true/bool etc - see openapi spec)
        return (bool) $requestParamData['value'];
    }
}
