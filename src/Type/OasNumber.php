<?php

declare(strict_types=1);

namespace ApiX\Type;

class OasNumber extends OasType
{
    public function __construct(
        public readonly NumberFormat|string|null $format = null,
    ) {
    }

    public function value(array $requestParamData): float
    {
        // TODO - check if 'value' is numeric etc.
        return (float) $requestParamData['value'];
    }
}
