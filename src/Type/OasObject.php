<?php

declare(strict_types=1);

namespace ApiX\Type;

class OasObject extends OasType
{
    /**
     * @param array<string, OasType> $properties
     */
    public function __construct(
        public readonly array $properties,
    ) {
    }

    public function value(array $requestParamData): array
    {
        // TODO - cast to properties types
        return $requestParamData['value'];
    }
}
