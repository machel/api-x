<?php

declare(strict_types=1);

namespace ApiX\Type;

class Ref extends OasType
{
    public function __construct(
        public readonly string $typeReference   // '#/components/schemas/Group' or 'Classname::class'
    ) {
    }

    public function value(array $requestParamData): array
    {
        // TODO - cast to items type
        return $requestParamData['value'];
    }
}
