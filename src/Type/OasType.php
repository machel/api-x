<?php

declare(strict_types=1);

namespace ApiX\Type;

abstract class OasType
{
    /**
     * @param int[]|float[]|string[]|array[]|null $oasAllowed
     */
    protected function __construct(
        protected readonly FormatInterface|string|null $oasFormat = null,
        protected readonly array|null $oasAllowed = null,
        protected readonly bool|int|float|string|array|null $oasDefault = null,
    ) {
    }

    abstract public function value(array $requestParamData): mixed;

    public function allowedValues(): array|null
    {
        return $this->oasAllowed;
    }

    public function getDefaultBool(): bool|null
    {
        return is_bool($this->oasDefault) ? $this->oasDefault : null;
    }

    public function getDefaultInt(): int|null
    {
        return is_int($this->oasDefault) ? $this->oasDefault : null;
    }

    public function getDefaultFloat(): float|null
    {
        return is_float($this->oasDefault) ? $this->oasDefault : null;
    }

    public function getDefaultString(): string|null
    {
        return is_string($this->oasDefault) ? $this->oasDefault : null;
    }

    public function getDefaultArray(): array|null
    {
        return is_array($this->oasDefault) ? $this->oasDefault : null;
    }
}
