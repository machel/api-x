<?php

declare(strict_types=1);

namespace ApiX\Type;

class AnyOf extends OasType
{
    /**
     * @var OasType[]
     */
    public readonly array $types;

    public function __construct(
        OasType ...$types
    ) {
        $this->types = $types;
    }

    public function value(array $requestParamData): array
    {
        // TODO - cast to items type
        return $requestParamData['value'];
    }
}
