<?php

declare(strict_types=1);

namespace ApiX\Type;

enum StringFormat: string implements FormatInterface
{
    case DEFAULT = '-';
    case BYTE = 'byte';             // base64 encoded characters
    case BINARY = 'binary';         // any sequence of octets
    case DATE = 'date';             // as defined by full-date - RFC3339
    case DATE_TIME = 'date-time';   // as defined by date-time - RFC3339
    case PASSWORD = 'password';     // a hint to UIs to obscure input

    // not defined by OpenApi:
    case EMAIL = 'email';
    case UUID = 'uuid';
    case URI = 'uri';
    case HOSTNAME = 'hostname';
    case IPV4 = 'ipv4';
    case IPV6 = 'ipv6';
}

