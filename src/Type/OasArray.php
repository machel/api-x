<?php

declare(strict_types=1);

namespace ApiX\Type;

use ApiX\Required;

class OasArray extends OasType
{
    public function __construct(
        public readonly OasType $itemsType,
        public readonly Required|null $required = null,
    ) {
    }

    public function value(array $requestParamData): array
    {
        // TODO - cast to items type
        return $requestParamData['value'];
    }
}
