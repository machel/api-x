<?php

declare(strict_types=1);

namespace ApiX\Type;

enum IntegerFormat: string implements FormatInterface
{
    case DEFAULT = '-';
    case INT32 = 'int32';   // signed 32 bits
    case INT64 = 'int64';   // signed 64 bits (aka long
}
