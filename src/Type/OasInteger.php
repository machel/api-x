<?php

declare(strict_types=1);

namespace ApiX\Type;

class OasInteger extends OasType
{
    public function __construct(
        public readonly IntegerFormat|string|null $format = null,
    ) {
    }

    public function value(array $requestParamData): int
    {
        // TODO - check if 'value' is numeric etc.
        return (int) $requestParamData['value'];
    }
}
