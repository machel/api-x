<?php

declare(strict_types=1);

namespace ApiX\Type;

class OasString extends OasType
{
    /**
     * @param string[]|null $allowed
     */
    public function __construct(
        public readonly StringFormat|string|null $format = null,
        public readonly array|null $allowed = null,
        public readonly string|null $default = null,
    ) {
        parent::__construct($this->format, $this->allowed, $this->default);
    }

    public function value(array $requestParamData): string
    {
        // TODO - check if value is string, maybe other checks/sanitization
        return (string) $requestParamData['value'];
    }
}
