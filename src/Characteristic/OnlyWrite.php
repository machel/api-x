<?php

declare(strict_types=1);

namespace ApiX;

enum OnlyWrite: int
{
    case TRUE = 1;
    case FALSE = 0;
}
