<?php

declare(strict_types=1);

namespace ApiX;

enum In: string
{
    case QUERY = 'query';
    case PATH = 'path';
    case HEADER = 'header';
    case COOKIE = 'cookie';
}
