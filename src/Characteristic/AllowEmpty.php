<?php

declare(strict_types=1);

namespace ApiX;

enum AllowEmpty
{
    case TRUE;
    case FALSE;
}
