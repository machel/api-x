<?php

declare(strict_types=1);

namespace ApiX;

enum Required
{
    case TRUE;
    case FALSE;
}
