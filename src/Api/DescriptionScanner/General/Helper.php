<?php

declare(strict_types=1);

namespace ApiX\Api\DescriptionScanner\General;

class Helper
{
    /**
     * @return string URL's path without uriPath specific parts (like query part after '?')
     */
    public static function uriPathToUrl(string $uriPath): string
    {
        return explode('?', $uriPath, 2)[0];
    }
}
