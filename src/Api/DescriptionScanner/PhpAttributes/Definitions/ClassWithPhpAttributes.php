<?php

declare(strict_types=1);

namespace ApiX\Api\DescriptionScanner\PhpAttributes\Definitions;

use ApiX\Endpoint;
use ApiX\OperationType;
use ApiX\Parameters;
use ApiX\Responses;

/**
 * Class with php attributes' instances annotating it
 */
class ClassWithPhpAttributes
{
    public readonly Parameters $parameters;

    public function __construct(
        public readonly string $classname,
        public readonly \ReflectionClass $reflector,
        public readonly RouteInfo $routeInfo,
        public readonly Endpoint $endpoint,
        public readonly Operations $operations,
        public readonly Responses $responses,
        Parameters|null $parameters,
    ) {
        if ($parameters === null) {
            $parameters = new Parameters([]);
        }
        $this->parameters = $parameters;
    }

    public function handlerNameForOperation(OperationType $operationType): string|null
    {
        $operation = $this->operations->getOperation($operationType);

        if ($operation === null) {
            return null;
        }

        return $operation->functionName;
    }
}
