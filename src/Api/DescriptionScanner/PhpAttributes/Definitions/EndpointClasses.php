<?php

declare(strict_types=1);

namespace ApiX\Api\DescriptionScanner\PhpAttributes\Definitions;

use ApiX\Tools\ArrayCollectionTrait;

class EndpointClasses implements \IteratorAggregate
{
    /**
     * @use ArrayCollectionTrait<ClassWithPhpAttributes>
     */
    use ArrayCollectionTrait;

    public function addEndpointClass(ClassWithPhpAttributes $endpointClass): void
    {
        $this->append($endpointClass);
    }

    public function getClass($classname): ClassWithPhpAttributes|null
    {
        /** @var ClassWithPhpAttributes $endpointClass */
        foreach ($this as $endpointClass) {
            if ($endpointClass->classname === $classname) {
                return $endpointClass;
            }
        }

        return null;
    }
}
