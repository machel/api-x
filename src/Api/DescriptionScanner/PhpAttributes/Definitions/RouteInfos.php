<?php

declare(strict_types=1);

namespace ApiX\Api\DescriptionScanner\PhpAttributes\Definitions;

use ApiX\Tools\ArrayCollectionTrait;

class RouteInfos implements \IteratorAggregate
{
    /**
     * @use ArrayCollectionTrait<RouteInfo>
     */
    use ArrayCollectionTrait;

    public function addRouteInfo(RouteInfo $routeInfo): void
    {
        $this->append($routeInfo);
    }
}
