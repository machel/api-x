<?php

declare(strict_types=1);

namespace ApiX\Api\DescriptionScanner\PhpAttributes\Definitions;

use ApiX\Operation;
use ApiX\OperationType;
use ApiX\Tools\ArrayCollectionTrait;

class Operations implements \IteratorAggregate, \Countable
{
    /**
     * @use ArrayCollectionTrait<Operation>
     */
    use ArrayCollectionTrait;

    public function addOperation(Operation $operation): void
    {
        $this->append($operation);
    }

    public function getOperation(OperationType $operationType): Operation|null
    {
        /** @var Operation $operation */
        foreach ($this as $operation) {
            if ($operation->operationType === $operationType) {
                return $operation;
            }
        }

        return null;
    }
}
