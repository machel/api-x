<?php

declare(strict_types=1);

namespace ApiX\Api\DescriptionScanner\PhpAttributes\Definitions;

use ApiX\Api\DescriptionScanner\General\Helper;

class RouteInfo
{
    public readonly string $relativeUrl;

    /**
     * @param string[] $httpMethods uppercase, ex: ['GET', 'POST']
     */
    public function __construct(
        public readonly array $httpMethods,
        public readonly string $uriPath,
        public readonly string $classname,
        string $relativeUrl = '',
    ) {
        if ($relativeUrl === '') {
            $relativeUrl = Helper::uriPathToUrl($this->uriPath);
        }
        $this->relativeUrl = $relativeUrl;
    }
}
