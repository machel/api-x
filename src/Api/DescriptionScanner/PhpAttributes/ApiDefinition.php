<?php

declare(strict_types=1);

namespace ApiX\Api\DescriptionScanner\PhpAttributes;

use ApiX\Api\DescriptionScanner\PhpAttributes\Definitions\EndpointClasses;
use ApiX\Api\DescriptionScanner\PhpAttributes\Definitions\RouteInfos;

class ApiDefinition
{
    public function __construct(
        public readonly EndpointClasses $endpointClasses,
        public readonly RouteInfos $routeInfos,
    ) {
    }
}
