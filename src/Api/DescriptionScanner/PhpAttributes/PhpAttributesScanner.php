<?php

declare(strict_types=1);

namespace ApiX\Api\DescriptionScanner\PhpAttributes;

use ApiX\Api\DescriptionScanner\PhpAttributes\Definitions\ClassWithPhpAttributes;
use ApiX\Api\DescriptionScanner\PhpAttributes\Definitions\EndpointClasses;
use ApiX\Api\DescriptionScanner\PhpAttributes\Definitions\Operations;
use ApiX\Api\DescriptionScanner\PhpAttributes\Definitions\RouteInfo;
use ApiX\Api\DescriptionScanner\PhpAttributes\Definitions\RouteInfos;
use ApiX\Api\Errors\GeneralException;
use ApiX\Api\Tools\Psr4ClassReader;
use ApiX\Config;
use ApiX\Endpoint;
use ApiX\Operation;
use ApiX\Parameters;
use ApiX\Responses;

class PhpAttributesScanner
{
    /**
     * @param array<string, string> $dirsToScan
     *  key - namespace
     *  value - directory pathname
     */
    public function scanPsr4Directories(array $dirsToScan): ApiDefinition
    {
        $classnames = Psr4ClassReader::readClassnames($dirsToScan);
        return $this->scanClassnames($classnames);
    }

    /**
     * @param string[] $classnames
     */
    protected function scanClassnames(array $classnames): ApiDefinition
    {
        $routeInfos = new RouteInfos();
        $endpointClasses = new EndpointClasses();

        foreach ($classnames as $classname) {

            $reflector = $this->lookForEndpointClass($classname);
            if ($reflector === null) {
                continue;
            }

            /** @var Endpoint $apixEndpoint */
            $apixEndpoint = $reflector->getAttributes(Config::ENDPOINT_ATTRIBUTE_CLASS)[0]->newInstance();

            /** @var Parameters|null $apixParameters */
            $apixParameters = null;

            $parametersAttrs = $reflector->getAttributes(Config::PARAMETERS_ATTRIBUTE_CLASS);
            if (!empty($parametersAttrs)) {
                $apixParameters = $parametersAttrs[0]->newInstance();
            }

            /** @var Responses $apixResponses */
            $apixResponses = null;

            $responsesAttrs = $reflector->getAttributes(Config::RESPONSES_ATTRIBUTE_CLASS);
            if (empty($responsesAttrs)) {
                throw new GeneralException("Class {$classname} must have responses defined");
            }
            $apixResponses = $responsesAttrs[0]->newInstance();

            $apixOperations = new Operations();

            /** @var string[] $httpMethods */
            $httpMethods = []; // ex: ['GET', 'POST']

            foreach ($reflector->getMethods() as $methodRefl) {
                foreach ($methodRefl->getAttributes() as $methodAttr) {

                    /** @var Operation $apixOperation */
                    $apixOperation = $methodAttr->newInstance();

                    if (!is_subclass_of($apixOperation, Config::OPERATION_ATTRIBUTE_CLASS)) {
                        continue;
                    }

                    $apixOperation->setFunctionName($methodRefl->getName());
                    $apixOperations->addOperation($apixOperation);
                    $httpMethods[] = $apixOperation->operationType->value;
                }
            }

            if (count($apixOperations) < 1) {
                throw new GeneralException("Class {$classname} must have at least one operation defined");
            }

            $routeInfo = new RouteInfo($httpMethods, $apixEndpoint->uriPath, $classname);

            $routeInfos->addRouteInfo($routeInfo);

            $endpointClasses->addEndpointClass(new ClassWithPhpAttributes(
                $classname,
                $reflector,
                $routeInfo,
                $apixEndpoint,
                $apixOperations,
                $apixResponses,
                $apixParameters
            ));
        }

        return new ApiDefinition($endpointClasses, $routeInfos);
    }

    /**
     * @param string[] $classnames
     * @return ClassWithPhpAttributes[]
     */
    protected function extractEndpointClasses(array $classnames): array
    {
        /**
         * @var ClassWithPhpAttributes[] $classesWithReflectors
         */
        $classesWithReflectors = [];

        foreach ($classnames as $classname) {
            $reflector = $this->lookForEndpointClass($classname);
            if ($reflector === null) {
                continue;
            }


            $classesWithReflectors[] = $reflector;
        }

        return $classesWithReflectors;
    }

    protected function lookForEndpointClass(string $classname): \ReflectionClass|null
    {
        $reflector = new \ReflectionClass($classname);
        $attrsEndpoint = $reflector->getAttributes(Config::ENDPOINT_ATTRIBUTE_CLASS);

        if (empty($attrsEndpoint)) {
            return null;
        }

        return $reflector;
    }
}
