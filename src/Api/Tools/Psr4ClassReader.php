<?php

declare(strict_types=1);

namespace ApiX\Api\Tools;

/**
 * Reads fully qualified classnames from given directories.
 * Directory, file and class names and structure must conform to PSR-4 specification.
 * Classes will be loaded upon read.
 */
class Psr4ClassReader
{
    /**
     * @param array<string, string> $nsToDirsMap
     *  key - namespace
     *  value - directory pathname
     *
     * @return string[] classnames
     */
    public static function readClassnames(array $nsToDirsMap): array
    {
        $classnames = [];

        foreach ($nsToDirsMap as $namespace => $dirpath) {
            $classnames += self::readDirClassnames($namespace, $dirpath);
        }

        return $classnames;
    }

    private static function readDirClassnames(string $namespace, string $dirpath): array
    {
        /**
         * @var \RecursiveDirectoryIterator $iterator
         * @var \SplFileInfo $item
         */
        foreach ($iterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($dirpath, \RecursiveDirectoryIterator::SKIP_DOTS),
            \RecursiveIteratorIterator::SELF_FIRST
        ) as $item) {
            if ($item->isFile() && $item->getExtension() === 'php') {
                include_once $item->getRealPath();
            }
        }

        return array_filter(get_declared_classes(), static function($classname) use($namespace): bool {
            return str_starts_with($classname, $namespace);
        });
    }
}
