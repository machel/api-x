<?php

declare(strict_types=1);

namespace ApiX\Api\Serialize\Converter;

use ApiX\AllowEmpty;
use ApiX\Api\Errors\GeneralException;
use ApiX\Api\Integrator\Value;
use ApiX\Api\Integrator\ValueType;
use ApiX\Parameter;
use ApiX\Type\OasArray;
use ApiX\Type\OasObject;
use ApiX\Type\OasType;

abstract class BaseConverter
{
    abstract protected function getParameterDefaultValue(Parameter $apixParameter): bool|int|float|string|array|null;
    abstract protected function convertAndValidateParameterValue(Value $value, Parameter $apixParameter): Value;

    public function convertAndValidate(Value $value, Parameter $apixParameter): Value
    {
        $convertedVal = $value->getRaw();

        if (!$value->isFound()) {
            if (($def = $this->getParameterDefaultValue($apixParameter)) !== null) {
                $convertedVal = $def;
            } elseif ($apixParameter->required) {
                throw new GeneralException("Parameter '{$apixParameter->name}' is required.");
            } else {
                return new Value(null, ValueType::NOT_FOUND);
            }
        }

        if ($value->isEmpty()) {
            if (($def = $this->getParameterDefaultValue($apixParameter)) !== null) {
                $convertedVal = $def;
            } elseif ($apixParameter->allowEmpty === AllowEmpty::TRUE) {
                return new Value('', ValueType::EMPTY);
            } elseif ($apixParameter->required) {
                throw new GeneralException("Parameter '{$apixParameter->name}' can't be empty.");
            } else {
                return new Value(null, ValueType::NOT_FOUND);
            }
        }

        if (!$this->isTypeComplyWithDefined($value->type, $apixParameter->type)) {
            throw new GeneralException("Type of '{$apixParameter->name}' is invalid.");
        }

        return $this->convertAndValidateParameterValue(new Value($convertedVal, $value->type), $apixParameter);

    }

    protected function isTypeComplyWithDefined(ValueType $valueType, OasType $parameterType): bool
    {
        if ($valueType === ValueType::NOT_FOUND || $valueType === ValueType::EMPTY) {
            return false;
        }

        if (
            $valueType === ValueType::OBJECT
            && !($parameterType instanceof OasObject)
        ) {
            return false;
        }

        if (
            $valueType === ValueType::ARRAY
            && !($parameterType instanceof OasArray)
        ) {
            return false;
        }

        return true;
    }
}
