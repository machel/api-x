<?php

declare(strict_types=1);

namespace ApiX\Api\Serialize\Converter;

use ApiX\Api\Integrator\Value;
use ApiX\Parameter;

class ConvertObject extends BaseConverter
{
    protected function getParameterDefaultValue(Parameter $apixParameter): array
    {
        return $apixParameter->type->getDefaultArray();
    }

    protected function convertAndValidateParameterValue(Value $value, Parameter $apixParameter): Value
    {
        /** @var array|null $convertedVal */
        $convertedVal = $value->getAsArray();
        $convertedType = $value->type;

        // cast to string
        // validate against StringFormat
        // validate against allowed values

        return new Value($convertedVal, $convertedType);
    }
}
