<?php

declare(strict_types=1);

namespace ApiX\Api\Serialize\Converter;

use ApiX\Api\Integrator\Value;
use ApiX\Parameter;

class ConvertInteger extends BaseConverter
{
    protected function getParameterDefaultValue(Parameter $apixParameter): int
    {
        return $apixParameter->type->getDefaultInt();
    }

    protected function convertAndValidateParameterValue(Value $value, Parameter $apixParameter): Value
    {
        /** @var int|null $convertedVal */
        $convertedVal = $value->getAsInt();
        $convertedType = $value->type;

        // cast to string
        // validate against StringFormat
        // validate against allowed values

        return new Value($convertedVal, $convertedType);
    }
}
