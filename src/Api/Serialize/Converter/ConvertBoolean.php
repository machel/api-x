<?php

declare(strict_types=1);

namespace ApiX\Api\Serialize\Converter;

use ApiX\Api\Integrator\Value;
use ApiX\Parameter;

class ConvertBoolean extends BaseConverter
{
    protected function getParameterDefaultValue(Parameter $apixParameter): bool|int|float|string|array|null
    {
        return $apixParameter->type->getDefaultBool();
    }

    protected function convertAndValidateParameterValue(Value $value, Parameter $apixParameter): Value
    {
        /** @var bool|null $convertedVal */
        $convertedVal = $value->getAsBool();
        $convertedType = $value->type;

        // cast to string
        // validate against StringFormat
        // validate against allowed values

        return new Value($convertedVal, $convertedType);
    }
}
