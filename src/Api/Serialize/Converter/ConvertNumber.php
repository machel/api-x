<?php

declare(strict_types=1);

namespace ApiX\Api\Serialize\Converter;

use ApiX\Api\Integrator\Value;
use ApiX\Parameter;

class ConvertNumber extends BaseConverter
{
    protected function getParameterDefaultValue(Parameter $apixParameter): float
    {
        return $apixParameter->type->getDefaultFloat();
    }

    protected function convertAndValidateParameterValue(Value $value, Parameter $apixParameter): Value
    {
        /** @var float|null $convertedVal */
        $convertedVal = $value->getAsFloat();
        $convertedType = $value->type;

        // cast to string
        // validate against StringFormat
        // validate against allowed values

        return new Value($convertedVal, $convertedType);
    }
}
