<?php

declare(strict_types=1);

namespace ApiX\Api\Serialize\Converter;

use ApiX\Api\Integrator\Value;
use ApiX\Parameter;
use ApiX\Type\OasArray;
use ApiX\Type\OasBoolean;
use ApiX\Type\OasInteger;
use ApiX\Type\OasNumber;
use ApiX\Type\OasObject;
use ApiX\Type\OasString;

class ConversionHub
{
    /**
     * @var BaseConverter[]
     */
    protected array $converters;

    public function convertAndValidate(Value $value, Parameter $apixParameter): Value
    {
        $class = match (get_class($apixParameter->type)) {
            OasObject::class => ConvertObject::class,
            OasArray::class => ConvertArray::class,
            OasBoolean::class => ConvertBoolean::class,
            OasInteger::class => ConvertInteger::class,
            OasNumber::class => ConvertNumber::class,
            OasString::class => ConvertString::class,
        };

        if (!isset($this->converters[$class])) {
            $this->converters[$class] = new $class();
        }

        return $this->converters[$class]->convertAndValidate($value, $apixParameter);
    }
}
