<?php

declare(strict_types=1);

namespace ApiX\Api\Serialize\Converter;

use ApiX\Api\Errors\GeneralException;
use ApiX\Api\Integrator\Value;
use ApiX\Parameter;

class ConvertString extends BaseConverter
{
    protected function getParameterDefaultValue(Parameter $apixParameter): string
    {
        return $apixParameter->type->getDefaultString();
    }

    protected function convertAndValidateParameterValue(Value $value, Parameter $apixParameter): Value
    {
        /** @var string|null $convertedVal */
        $convertedVal = $value->getAsString();
        $convertedType = $value->type;

        if ($convertedVal === null) {
            throw new GeneralException("Fatal:: incomprehensible null in '{$apixParameter->name}' parameter");
        }

        // TODO:
        //   use format validator to validate against StringFormat

        if (!empty($allowedValues = $apixParameter->type->allowedValues())) {
            if (!in_array($convertedVal, $allowedValues, true)) {
                throw new GeneralException("'{$apixParameter->name}' parameter's value not allowed");
            }
        }

        return new Value($convertedVal, $convertedType);
    }
}
