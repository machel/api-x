<?php

declare(strict_types=1);

namespace ApiX\Api\Serialize\Serializer;

use ApiX\Api\Integrator\Parameters;
use ApiX\Api\Integrator\Value;
use ApiX\Api\Integrator\ValueType;
use ApiX\Api\Serialize\Serializer\SerializePsrMessage\InCookie;
use ApiX\Api\Serialize\Serializer\SerializePsrMessage\InHeader;
use ApiX\Api\Serialize\Serializer\SerializePsrMessage\InPath;
use ApiX\Api\Serialize\Serializer\SerializePsrMessage\InQuery;
use ApiX\Api\Serialize\Converter\ConversionHub;
use ApiX\In;
use ApiX\Parameter;
use ApiX\Type\OasArray;
use ApiX\Type\OasObject;
use Psr\Http\Message\ServerRequestInterface;

class PsrMessageSerializer
{
    /**
     * @var SerializerInterface[]
     */
    protected array $serializers;
    protected readonly ConversionHub $converter;

    public function __construct()
    {
        $this->converter = new ConversionHub();
    }

    /**
     * @param Parameter[] $apixParameters
     */
    public function deserializeRequestParams(array $apixParameters, ServerRequestInterface $request): Parameters
    {
        $parameters = new Parameters();

        foreach ($apixParameters as $apixParameter) {
            $clonedParameter = clone $apixParameter;

            $value = $this->fetchParamValue($clonedParameter, $request);
            $convertedValue = $this->converter->convertAndValidate($value, $clonedParameter);
            $clonedParameter->setParameterValue($convertedValue);

            $parameters->addParameter($apixParameter->name, $clonedParameter);
        }

        return $parameters;
    }

    public function fetchParamValue(Parameter $apixParameter, ServerRequestInterface $request): Value
    {
        // from where-in
        // serialize format: style + explode
        // get as string
        // convert to proper type

//        dump($request->getAttributes());
//        dump($request->getQueryParams());
//        dump($request->getHeaders());
//        dump($request->getCookieParams());
//        die();

        /** @var string|null $requestVal */
        $requestVal = $request->getAttribute($apixParameter->name);

        if ($requestVal === null) {
            return new Value(null, ValueType::NOT_FOUND);
        }

        // TODO - allow Apix/Parameter define custom serializer class

        $defaultClass = [
            In::PATH->name => InPath\SimpleStyleObjectSerializer::class,
            In::QUERY->name => InQuery\FormStyleObjectSerializer::class,
            In::HEADER->name => InHeader\SimpleStyleObjectSerializer::class,
            In::COOKIE->name => InCookie\FormStyleObjectSerializer::class,
        ];

        $style = "{$apixParameter->in->name}_{$apixParameter->serializationFormat?->name}";

        if ($apixParameter->type instanceof OasObject) {
            $style .= '_OBJECT';
        } elseif ($apixParameter->type instanceof OasArray) {
            $style .= '_ARRAY';
        } else {
            $style .= '_PRIMITIVE';
        }

        $serializerClass = match ($style) {
            'PATH_STYLE_SIMPLE_OBJECT' => InPath\SimpleStyleObjectSerializer::class,
            'PATH_STYLE_SIMPLE_ARRAY' => InPath\SimpleStyleArraySerializer::class,
            'PATH_STYLE_SIMPLE_PRIMITIVE' => InPath\SimpleStylePrimitiveSerializer::class,
            'QUERY_STYLE_FORM_OBJECT' => InQuery\FormStyleObjectSerializer::class,
            'QUERY_STYLE_FORM_ARRAY' => InQuery\FormStyleArraySerializer::class,
            'QUERY_STYLE_FORM_PRIMITIVE' => InQuery\FormStylePrimitiveSerializer::class,
            'HEADER_STYLE_SIMPLE_OBJECT' => InHeader\SimpleStyleObjectSerializer::class,
            'HEADER_STYLE_SIMPLE_ARRAY' => InHeader\SimpleStyleArraySerializer::class,
            'HEADER_STYLE_SIMPLE_PRIMITIVE' => InHeader\SimpleStylePrimitiveSerializer::class,
            'COOKIE_STYLE_FORM_OBJECT' => InCookie\FormStyleObjectSerializer::class,
            'COOKIE_STYLE_FORM_ARRAY' => InCookie\FormStyleArraySerializer::class,
            'COOKIE_STYLE_FORM_PRIMITIVE' => InCookie\FormStylePrimitiveSerializer::class,
            default => $defaultClass[$apixParameter->in->name],
        };

        if (!isset($this->serializers[$serializerClass])) {
            $this->serializers[$serializerClass] = new $serializerClass();
        }

        // value from request into Value object
        return $this->serializers[$serializerClass]->deserialize($requestVal);
    }
}

// PATH
// Array
// 1,2,3 => "1,2,3"
// .3,4,5 => ".3,4,5"
// .3.4.5 => ".3.4.5"
// ;id=3,4,5 => ";id=3,4,5"
// ;id=3;id=4;id=5 => ";id=3;id=4;id=5"
// Object
// role,admin,firstName,Alex => "role,admin,firstName,Alex"
// role=admin,firstName=Alex => "role=admin,firstName=Alex"
// .role,admin,firstName,Alex => ".role,admin,firstName,Alex"
// .role=admin.firstName=Alex => ".role=admin.firstName=Alex"
// ;id=role,admin,firstName,Alex => ";id=role,admin,firstName,Alex"
// ;role=admin;firstName=Alex => ";role=admin;firstName=Alex"

// QUERY
// Array
// id=3&id=4&id=5 => "5"
// id=3,4,5 => "3,4,5"
// id=3%204%205 => "3 4 5"
// id=3|4|5 => "3|4|5"
// Object
// role=admin&firstName=Alex => ["role" => "admin", "firstName" => "Alex"]
// id=role,admin,firstName,Alex => ["id" => "role,admin,firstName,Alex"]
// id[role]=admin&id[firstName]=Alex => ["id" => [ "role" => "admin" "firstName" => "Alex" ]]
