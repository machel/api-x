<?php

declare(strict_types=1);

namespace ApiX\Api\Serialize\Serializer\SerializePsrMessage\InPath;

use ApiX\Api\Integrator\Value;
use ApiX\Api\Integrator\ValueType;
use ApiX\Api\Serialize\Serializer\SerializerInterface;

class SimpleStyleObjectSerializer implements SerializerInterface
{
    /**
     * /users/5
     * /users/3,4,5
     * /users/role,admin,firstName,Alex
     * /users/role.type,admin,role.level,12,firstName,Alex
     */
    public function deserialize(string $value): Value
    {
        $deserializedValue = [];
        $valueParts = explode(',', $value);
        $countValParts = count($valueParts);

        for ($i = 0; $i < $countValParts; $i += 2) {
            $key = $valueParts[$i];

            if (str_contains($key, '.')) {
                // we have object
                $keyParts = explode('.', $key);
                $countKParts = count($keyParts);
                $lastArrEl = &$deserializedValue;
                for ($partI = 0; $partI < $countKParts - 1; $partI++) {
                    $arrKey = $keyParts[$partI];
                    if (!isset($lastArrEl[$arrKey])) {
                        $lastArrEl[$arrKey] = [];
                    }
                    $lastArrEl = &$lastArrEl[$arrKey];
                }
                $lastArrEl[$keyParts[$countKParts - 1]] = $valueParts[$i + 1];
                continue;
            }

            $val = $valueParts[$i + 1] ?? null;
            $deserializedValue[$key] = $val;
        }

        return new Value($deserializedValue, ValueType::OBJECT);
    }
}
