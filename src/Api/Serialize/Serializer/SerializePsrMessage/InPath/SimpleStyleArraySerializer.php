<?php

declare(strict_types=1);

namespace ApiX\Api\Serialize\Serializer\SerializePsrMessage\InPath;

use ApiX\Api\Integrator\Value;
use ApiX\Api\Integrator\ValueType;
use ApiX\Api\Serialize\Serializer\SerializerInterface;

class SimpleStyleArraySerializer implements SerializerInterface
{
    /**
     * /users/5
     * /users/3,4,5
     * /users/role,admin,firstName,Alex
     * /users/role.type,admin,role.level,12,firstName,Alex
     */
    public function deserialize(string $value): Value
    {
        $deserializedValue = explode(',', $value);
        return new Value($deserializedValue, ValueType::ARRAY);
    }
}
