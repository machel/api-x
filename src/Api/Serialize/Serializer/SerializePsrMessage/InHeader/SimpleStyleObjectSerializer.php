<?php

declare(strict_types=1);

namespace ApiX\Api\Serialize\Serializer\SerializePsrMessage\InHeader;

use ApiX\Api\Integrator\Value;
use ApiX\Api\Integrator\ValueType;
use ApiX\Api\Serialize\Serializer\SerializerInterface;

class SimpleStyleObjectSerializer implements SerializerInterface
{
    public function deserialize(string $value): Value
    {
        return new Value(null, ValueType::NOT_FOUND);
    }
}
