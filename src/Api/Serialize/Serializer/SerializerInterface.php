<?php

declare(strict_types=1);

namespace ApiX\Api\Serialize\Serializer;

use ApiX\Api\Integrator\Value;
use ApiX\Parameter;

interface SerializerInterface
{
    public function deserialize(string $value): Value;
}
