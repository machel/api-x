<?php

declare(strict_types=1);

namespace ApiX\Api\Integrator\FrameworkX;

use ApiX\Api\DescriptionScanner\PhpAttributes\Definitions\RouteInfo;
use ApiX\Api\DescriptionScanner\PhpAttributes\PhpAttributesScanner;
use FrameworkX\App;

class Bridge
{
    public static SourceAdapter $sourceAdapter;

    public static function fromPhpAttributes(array $namespaceToDirectoryMap, App $app): void
    {
        $scanner = new PhpAttributesScanner();

        $phpAttrsApiDefinition = $scanner->scanPsr4Directories($namespaceToDirectoryMap);

        self::$sourceAdapter = new PhpAttributesAdapter($phpAttrsApiDefinition);

        self::integrate($app, self::$sourceAdapter);
    }

    protected static function integrate(App $app, SourceAdapter $sourceAdapter): void
    {
        /** @var RouteInfo $routeInfo */
        foreach ($sourceAdapter->routeInfos() as $routeInfo) {
            $app->map($routeInfo->httpMethods, $routeInfo->relativeUrl, $routeInfo->classname);
        }
    }
}
