<?php

declare(strict_types=1);

namespace ApiX\Api\Integrator\FrameworkX;

use ApiX\Api\DescriptionScanner\PhpAttributes\Definitions\RouteInfo;
use ApiX\Api\Integrator\Context;
use ApiX\Api\Resource;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

interface SourceAdapter
{
    /**
     * @return \Traversable<RouteInfo>
     */
    public function routeInfos(): \Traversable;

    public function handlerName(string $classname, string $httpMethod): string|null;

    public function createContext(string $classname, ServerRequestInterface $request): Context;

    public function createResponse(Resource $resource): ResponseInterface;

    public function handleException(\Throwable $e): ResponseInterface;
}
