<?php

declare(strict_types=1);

namespace ApiX\Api\Integrator\FrameworkX;

use ApiX\Api\DescriptionScanner\PhpAttributes\ApiDefinition;
use ApiX\Api\DescriptionScanner\PhpAttributes\Definitions\ClassWithPhpAttributes;
use ApiX\Api\Errors\GeneralException;
use ApiX\Api\Integrator\Context;
use ApiX\Api\Resource;
use ApiX\Api\Serialize\Serializer\PsrMessageSerializer;
use ApiX\OperationType;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use React\Http\Message\Response;

class PhpAttributesAdapter implements SourceAdapter
{
    protected readonly PsrMessageSerializer $psrMessageSerializer;

    public function __construct(
        protected readonly ApiDefinition $phpAttrsApiDefinition
    ) {
        $this->psrMessageSerializer = new PsrMessageSerializer();
    }

    public function routeInfos(): \Traversable
    {
        return $this->phpAttrsApiDefinition->routeInfos->getIterator();
    }

    public function handlerName(string $classname, string $httpMethod): string|null
    {
        $endpointClass = $this->getEndpointClass($classname);

        $handlerName = $endpointClass->handlerNameForOperation(OperationType::from(strtoupper($httpMethod)));
        if ($handlerName === null) {
            throw new GeneralException("Fatal:: no handler for http method '{$httpMethod}' in class '{$classname}'");
        }

        return $handlerName;
    }

    public function createContext(string $classname, ServerRequestInterface $request): Context
    {
        $endpointClass = $this->getEndpointClass($classname);

        $parameters = $this->psrMessageSerializer->deserializeRequestParams($endpointClass->parameters->parameters, $request);

        return new Context($parameters);

        // parameter
        // 1. param-picker: (in-where) get from proper place as AssocArr
        // 2. de-serializer: convert AssocArr to proper type
        // 3. validate

        // - parameters <- path,query,headers,cookies
        // - payload <- body
    }

    public function createResponse(Resource $resource): ResponseInterface
    {
        return Response::json(['stub' => 'created by PhpAttributesAdapter::createResponse']);
    }

    public function handleException(\Throwable $e): ResponseInterface
    {
        return Response::json(['error/stub' => 'created by PhpAttributesAdapter::handleException'])
            ->withStatus(Response::STATUS_INTERNAL_SERVER_ERROR);
    }

    protected function getEndpointClass(string $classname): ClassWithPhpAttributes
    {
        $endpointClass = $this->phpAttrsApiDefinition->endpointClasses->getClass($classname);

        if ($endpointClass === null) {
            throw new GeneralException("Fatal:: no endpoint class for classname '{$classname}'");
        }

        return $endpointClass;
    }
}
