<?php

declare(strict_types=1);

namespace ApiX\Api\Integrator\FrameworkX;

use ApiX\Api\Errors\GeneralException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

abstract class Controller
{
    final public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        $sourceAdapter = Bridge::$sourceAdapter;

        dump($request->getAttribute());
        dd($request->getQueryParams());

        $classname = static::class;

        try {
            $handlerName = $sourceAdapter->handlerName($classname, $method = $request->getMethod());
            if ($handlerName === null) {
                throw new GeneralException("Handler for method '{$method}' not defined but registered for class '{$classname}");
            }
            $apixContext = $sourceAdapter->createContext($classname, $request);
            $resource = $this->$handlerName($request, $apixContext);
            return $sourceAdapter->createResponse($resource);
        } catch (\Throwable $e) {
            // TODO - exception to response / display / convert to message (prod vs dev)
            dump($e);
            return $sourceAdapter->handleException($e);
        }
    }
}
