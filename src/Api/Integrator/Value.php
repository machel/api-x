<?php

declare(strict_types=1);

namespace ApiX\Api\Integrator;

class Value
{
    /**
     * @param string|string[]|array|null $value
     */
    public function __construct(
        private readonly string|array|null $value,
        public readonly ValueType $type,
    ) {
    }

    public function isFound(): bool
    {
        return $this->type !== ValueType::NOT_FOUND;
    }

    public function isEmpty(): bool
    {
        return $this->type === ValueType::EMPTY;
    }

    /**
     * @param string|null $keys dot separated, ex: 'user.name.firstName'
     *
     * @return string|string[]|array|null
     */
    public function getRaw(string|null $keys = null): string|array|null
    {
        return $this->getValueByKeys($keys);
    }

    /**
     * @param string|null $keys dot separated, ex: 'user.name.firstName'
     */
    public function getAsBool(string|null $keys = null): bool|null
    {
        $val = $this->getValueByKeys($keys);

        return ($val === null) ? null : (bool) $val;
    }

    public function getAsInt(string|null $keys = null): int|null
    {
        $val = $this->getValueByKeys($keys);

        return ($val === null) ? null : (int) $val;
    }

    public function getAsFloat(string|null $keys = null): float|null
    {
        $val = $this->getValueByKeys($keys);

        return ($val === null) ? null : (float) $val;
    }

    public function getAsString(string|null $keys = null): string|null
    {
        $val = $this->getValueByKeys($keys);

        return ($val === null) ? null : (string) $val;
    }

    public function getAsArray(string|null $keys = null): array|null
    {
        $val = $this->getValueByKeys($keys);

        return ($val === null) ? null : (array) $val;
    }

    /**
     * @return string|string[]|array|null
     */
    private function getValueByKeys(string|null $keys = null): string|array|null
    {
        if ($keys === null || $keys === '') {
            return $this->value;
        }

//        if (!is_array($this->val)) {
//            throw new GeneralException('Trying to access properties on non array or non object value');
//        }

        $keyParts = explode('.', $keys);
        $countKParts = count($keyParts);
        $currEl = $this->value;
        for ($partI = 0; $partI < $countKParts - 1; $partI++) {
            $key = $keyParts[$partI];
            if ($currEl === null) {
                return null;
            }
            $currEl = $currEl[$key];
//            if (!isset($currEl[$key])) {
//                throw new GeneralException("{$key} doesn't exist");
//            }
        }

        return $currEl[$keyParts[$countKParts - 1]] ?? null;
    }
}
