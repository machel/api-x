<?php

declare(strict_types=1);

namespace ApiX\Api\Integrator;

enum ValueType: int
{
    case NOT_FOUND = 0;
    case EMPTY = 1; // parameter was in request but without value set
    case PRIMITIVE = 2;
    case ARRAY = 3; // list
    case OBJECT = 4; // associative array
}
