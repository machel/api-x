<?php

declare(strict_types=1);

namespace ApiX\Api\Integrator;

use ApiX\Parameter;
use ApiX\Tools\MapCollectionTrait;

class Parameters implements \IteratorAggregate, \Countable
{
    /**
     * @use MapCollectionTrait<Parameter>
     */
    use MapCollectionTrait;

    public function addParameter(string $paramName, Parameter $parameter): void
    {
        $this->set($paramName, $parameter);
    }
}
