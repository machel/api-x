<?php

declare(strict_types=1);

namespace ApiX\Api\Integrator;

use ApiX\Api\Errors\GeneralException;
use ApiX\Parameter;

class Context
{
    public function __construct(
        protected readonly Parameters $parameters,
    ) {
    }

    public function parameter(string $name): Value
    {
        /** @var Parameter $apixParameter */
        $apixParameter = $this->parameters->get($name);

        if ($apixParameter === null) {
            throw new GeneralException("Parameter {$name} not defined.");
        }

        // Exten / string / int / null...
        return $apixParameter->getValue();
    }
}
