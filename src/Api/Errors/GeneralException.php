<?php

declare(strict_types=1);

namespace ApiX\Api\Errors;

/**
 * Temporary exception
 * TODO to be split into more specific exceptions
 */
class GeneralException extends \Exception
{
}
