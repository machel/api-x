<?php

declare(strict_types=1);

namespace ApiX;

readonly class Response
{
    public const DEFAULT = 'default';

    /**
     * @var string[]
     */
    public array $exceptionClasses;

    /**
     * @param SerializationFormat[] $serializationFormats
     */
    public function __construct(
        public string|null $description = null,
        public array $serializationFormats = [],
        string ...$exceptionClasses,
    ) {
        $this->exceptionClasses = $exceptionClasses;
    }
}
