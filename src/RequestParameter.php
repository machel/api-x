<?php

declare(strict_types=1);

namespace ApiX;

interface RequestParameter
{
    public function apiParameter(): Parameter;
    public function parameterValue(array $requestParamData): mixed;
}
