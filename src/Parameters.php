<?php

declare(strict_types=1);

namespace ApiX;

#[\Attribute(\Attribute::TARGET_CLASS)]
readonly class Parameters
{
    /**
     * @param Parameter[] $parameters
     */
    public function __construct(
        public array $parameters,
    ) {
    }
}
