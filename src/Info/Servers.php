<?php

declare(strict_types=1);

namespace ApiX;

#[\Attribute]
readonly class Servers
{
    /**
     * @var Server[] $servers
     */
    public array $servers;

    public function __construct(
        Server ...$servers
    ) {
        $this->servers = $servers;
    }
}
