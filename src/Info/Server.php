<?php

declare(strict_types=1);

namespace ApiX;

readonly class Server
{
    public function __construct(
        public string $url,
        public string|null $description = null,
    ) {
    }
}
