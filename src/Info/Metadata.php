<?php

declare(strict_types=1);

namespace ApiX;

#[\Attribute]
readonly class Metadata
{
    private const OPENAPI = '3.0.0';

    /*
        openapi: 3.0.0
        info:
          title: Sample API
          description: Optional multiline or single-line description in [CommonMark](http://commonmark.org/help/) or HTML.
          version: 0.1.9
          ...
     */
    public function __construct(
        public string $title,
        public string $version,
        public string|null $description = null,
    ) {
    }
}
