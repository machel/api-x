<?php

declare(strict_types=1);

namespace ApiX;

#[\Attribute(\Attribute::TARGET_CLASS)]
readonly class Endpoint
{
    public function __construct(
        public string $uriPath,
        public string $resourceClassname,
    ) {
    }
}
