<?php

declare(strict_types=1);

namespace ApiX;

enum SerializationFormat: string
{
    case APPLICATION_JSON = 'application/json';
    case APPLICATION_XML = 'application/xml';
    case APPLICATION_X_WWW_FORM = 'application/x-www-form-urlencoded';
    case STYLE_SIMPLE = 'simple';
//    case STYLE_SIMPLE_EXPLODE = 'simple_explode';
//    case STYLE_LABEL = 'label';
//    case STYLE_LABEL_EXPLODE = 'label_explode';
//    case STYLE_MATRIX = 'matrix';
//    case STYLE_MATRIX_EXPLODE = 'matrix_explode';
    case STYLE_FORM = 'form';
//    case STYLE_FORM_EXPLODE = 'form_explode';
//    case STYLE_SPACEDELIMITED = 'spaceDelimited';
//    case STYLE_SPACEDELIMITED_EXPLODE = 'spaceDelimited_explode';
//    case STYLE_PIPEDELIMITED = 'pipeDelimited';
//    case STYLE_PIPEDELIMITED_EXPLODE = 'pipeDelimited_explode';
//    case STYLE_DEEPOBJECT = 'deepObject';
}
