<?php

declare(strict_types=1);

namespace ApiX;

class Config
{
    public const ENDPOINT_ATTRIBUTE_CLASS = Endpoint::class;
    public const PARAMETERS_ATTRIBUTE_CLASS = Parameters::class;
    public const RESPONSES_ATTRIBUTE_CLASS = Responses::class;
    public const OPERATION_ATTRIBUTE_CLASS = Operation::class;
}
