<?php

declare(strict_types=1);

namespace ApiX;

#[\Attribute(\Attribute::TARGET_CLASS)]
readonly class Responses
{
    /**
     * @param array<string, Response> $responses
     */
    public function __construct(
        public array $responses,
    ) {
    }
}
