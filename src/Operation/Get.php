<?php

declare(strict_types=1);

namespace ApiX;

#[\Attribute(\Attribute::TARGET_METHOD)]
readonly class Get extends Operation
{
    protected function myOperationType(): OperationType
    {
        return OperationType::GET;
    }
}
