<?php

declare(strict_types=1);

namespace ApiX;

enum OperationType: string
{
    case GET = 'GET';
    case POST = 'POST';
    case PUT = 'PUT';

    /**
     * GET => ApiX\Get
     * POST => ApiX\Post
     * ...
     *
     * @return array<string, string>
     */
    public static function classnamesMap(): array
    {
        /**
         * @var array<string, string> $map
         */
        static $map = null;

        if ($map !== null) {
            return $map;
        }

        $map = [];

        foreach (self::cases() as $case) {
            $map[$case->name] = __NAMESPACE__ . '\\' . ucfirst(strtolower($case->name));
        }

        return $map;
    }

    public static function getClassnameFor(self $operationType): string
    {
        $map = self::classnamesMap();
        return $map[$operationType->name];
    }
}
