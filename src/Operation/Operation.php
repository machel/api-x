<?php

declare(strict_types=1);

namespace ApiX;

readonly abstract class Operation
{
    public OperationType $operationType;

    public string $functionName;

    public function __construct(
        public string|null $summary = null,
        public string|null $description = null,
        public string|null $operationId = null,
        public array|null $responses = null,
    ) {
        $this->operationType = $this->myOperationType();
    }

    abstract protected function myOperationType(): OperationType;

    public function setFunctionName(string $funName): void
    {
        $this->functionName = $funName;
    }
}
