<?php

declare(strict_types=1);

namespace ApiX\Tools;

/**
 * @template T
 *
 * Class using it must implement <\IteratorAggregate>
 */
trait ArrayCollectionTrait
{
    /** @var array<int, T> */
    private array $items = [];

    /**
     * @return \Traversable<T>
     */
    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->items);
    }

    /**
     * @param T $item
     */
    public function append(mixed $item): self
    {
        $this->items[] = $item;

        return $this;
    }

    public function count(): int
    {
        return count($this->items);
    }
}
