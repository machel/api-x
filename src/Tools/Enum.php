<?php

declare(strict_types=1);

namespace ApiX\Tools;

use UnitEnum;

class Enum
{
    /**
     * @param int|string $name // enum's name
     * @param UnitEnum[] $cases
     */
    public static function enumValue(int|string $name, array $cases): UnitEnum|null
    {
        $index = array_search($name, array_column($cases, "name"), true);

        if ($index !== false) {
            return $cases[$index];
        }

        return null;
    }

    /**
     * @param UnitEnum[] $cases
     */
    public static function inEnum(int|string $name, array $cases): bool
    {
        return self::enumValue($name, $cases) !== null;
    }
}
