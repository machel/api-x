<?php

declare(strict_types=1);

namespace ApiX\Tools;

/**
 * @template T
 *
 * Class using it must implement <\IteratorAggregate>
 */
trait MapCollectionTrait
{
    /** @var array<string, T> */
    protected array $items;

    /**
     * @return \Traversable<T>
     */
    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->items);
    }

    /**
     * @param T $item
     */
    public function set(string $key, mixed $item): self
    {
        $this->items[$key] = $item;

        return $this;
    }

    /**
     * @return T|null
     */
    public function get(string $key): mixed
    {
        return $this->items[$key] ?? null;
    }

    public function appendToItem(string $key, mixed $itemElement, string $elementKey = null): self
    {
        if (!isset($this->items[$key])) {
            $this->items[$key] = [];
        }

        if ($elementKey !== null) {
            $this->items[$key][$elementKey] = $itemElement;
        } else {
            $this->items[$key][] = $itemElement;
        }

        return $this;
    }

    public function count(): int
    {
        return count($this->items);
    }
}
