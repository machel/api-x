<?php

declare(strict_types=1);

namespace ApiX;

readonly class OpenapiOverride
{
    public function __construct(
        public array $definition
    ) {
    }
}
