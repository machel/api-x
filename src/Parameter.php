<?php

declare(strict_types=1);

namespace ApiX;

use ApiX\Api\Integrator\Value;
use ApiX\Type\OasType;

class Parameter
{
    protected readonly Value $value;

    public function __construct(
        public readonly string $name,
        public readonly In $in,
        public readonly Required $required,
        public readonly OasType $type,
        public readonly AllowEmpty $allowEmpty = AllowEmpty::FALSE,
        public readonly string|null $description = null,
        public readonly SerializationFormat|null $serializationFormat = null,
        public readonly OpenapiOverride|null $openapiOverride = null,
    ) {
    }

    public function setParameterValue(Value $value): void
    {
        $this->value = $value;
        $this->setValue($value);
    }

    public function getValue(): Value
    {
        return $this->value;
    }

    protected function setValue(Value $value): void
    {
    }
}
