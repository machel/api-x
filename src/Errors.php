<?php

declare(strict_types=1);

namespace ApiX;

use Throwable;

#[\Attribute(\Attribute::TARGET_CLASS)]
readonly class Errors
{
    /**
     * @param array<Throwable, int> $exceptionClassesMap
     */
    public function __construct(
        public array $exceptionClassesMap
    ) {
    }
}
