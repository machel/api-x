- OpenApi standard compliant
- asynchronous, non-blocking (based on Framework-X which is based on ReactPHP)

Roadmap
=======

- *DONE* prototype FrameworkX app
- *DONE* end-point serving resources described by openapi schema
    - full hand-made definition of resources - but in code (ex. array)
- [NOW] create openapi resources schema from annotated classes
    - infer schema from classes, attributes etc.
- implement exact, meaningful and verbose errors handling
- implement logging what application is doing (mainly bootstrap) - tracing?
- generated schema validation
- request parameters/body validation
- response data validation
- ApiPlatform try-out application to see how it works

## libs to consider
- [rw schema from/to array](https://github.com/cebe/php-openapi)

## validation libs to consider
- [json-schema](https://github.com/justinrainbow/json-schema)
- [respect](https://github.com/Respect/Validation)
- [laravel-like (rakit rewrite)](https://github.com/somnambulist-tech/validation)
  - build validation library on this? : validate objects, use php attributes
- [validation notifications]()

Later
=====
- php-cs-fixer / phpstan / phpmd / php_codesniffer (phpcs,phpcbf)  ;check, format
- auto style in IDE

- HTTP Cache: https://developer.mozilla.org/en-US/docs/Web/HTTP/Caching
- asynchronous redbean-like ORM

- TDD: unit tests, integration tests, functional tests, bdd?

- Test automation
- CI / CD
- stress tests



Project specification
=====================
```
SPEC-AGNOSTIC STRUCTURE
DescriptionScanner/
  assoc-array-describing-api -> [Extractor/Scanner]-> AssociativeArray/ApiDefinition
  php-attributes-describing-api -> [Extractor/Scanner]-> PhpAttributes/ApiDefinition

INTEGRATOR
AssocArr/Symfony:     ApiDefinition -> [AssocArrayAdapter] -> [Symfony/Bridge]
AssocArr/FrameworkX:  ApiDefinition -> [AssocArrayAdapter] -> [FrameworkX/Bridge]
PhpAttrs/FrameworkX:  ApiDefinition -> [PhpAttributesAdapter] -> [FrameworkX/Bridge]
```

```
TRANSFER-REQ/RES-DATA
Serialize/
  http-req-psr{param/body, ApixParameter (val)-> [PsrMessageSerializer]:convert(val>type) -> ApixParameter
  ApixParameter{value -> [ValidateManager]->[Validate<str/int/arr...>] -> throw ValidationErr


  http-req-psr{param/body -> [PsrMessageDataFetcher]-> DataContainer<AssocArr>{path/query/header/cookie/body
  DataContainer, [ApixParameter] -> [SerializeManager]
    (param-value,ApixParameter) -> [Serialize<str/int/arr...>]:convert(param-value) -> ApixParameter{value
  ApixParameter{value -> [ValidateManager]->[Validate<str/int/arr...>] -> throw ValidationErr
  
```





Handy/How-tos/References
========================
[reactphp/async](https://github.com/reactphp/async)
[fibers](https://clue.engineering/2021/fibers-in-php)
[HTTP Cache (Mozilla)](https://developer.mozilla.org/en-US/docs/Web/HTTP/Caching)
[psr15 middleware adapter](https://github.com/friends-of-reactphp/http-middleware-psr15-adapter)

[pretty DUMP](https://stackoverflow.com/questions/19816438/make-var-dump-look-pretty)
