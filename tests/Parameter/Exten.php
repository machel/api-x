<?php

declare(strict_types=1);

namespace ApiXTests\Parameter;

use ApiX\Api\Integrator\Value;
use ApiX\Parameter;

class Exten extends Parameter
{
    private int $id;

    /** @var float[] */
    private array $tags;

    public function __construct(
        string $name
    ) {
        parent::__construct(
            $name,
            \ApiX\In::QUERY,
            \ApiX\Required::FALSE,
            new \ApiX\Type\OasObject([
                'id' => new \ApiX\Type\OasInteger(),
                'tags' => new \ApiX\Type\OasArray(
                    new \ApiX\Type\OasNumber(\ApiX\Type\NumberFormat::FLOAT),
                ),
            ]),
        );
    }

    protected function setValue(Value $value): void
    {
//        dump($value);
//        $this->id = $value->getAsInt('id');
//        $this->tags = $value->getAsArray('tags');
//        dump($this->id);
//        dump($this->tags);
    }

//    public function getValue(): self
//    {
//        return $this;
//    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return float[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }
}
