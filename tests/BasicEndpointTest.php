<?php

declare(strict_types=1);

namespace ApiXTests;

use PHPUnit\Framework\TestCase;

#[\ApiX\Metadata(
    'ApiXApp API',
    '1.0.0',
    'Proof of concept and testing API for ApiX',
)]
#[\ApiX\Servers(
    new \ApiX\Server(
        'http://api.example.com/v1',
        'Optional server description, e.g. Main (production) server',
    ),
    new \ApiX\Server(
        'http://sandbox-api.example.com',
        'Optional server description, e.g. Internal staging server for testing',
    ),
)]
class BasicEndpointTest extends TestCase
{
    public function testItReturnsValidResponse(): void
    {
//        $get = new Get();
//        var_dump($get);
//
//        $this->assertInstanceOf(Get::class, $get);
    }
}
