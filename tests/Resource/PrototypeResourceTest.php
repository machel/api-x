<?php

declare(strict_types=1);

namespace ApiXTests\Resource;

use PHPUnit\Framework\TestCase;

class PrototypeResourceTest extends TestCase
{
    public function testOpenapiArrayIsProperlyCreated(): void
    {
        //$openapiArr = $this->openapi();
        //var_dump($openapiArr);

        $reflector = new \ReflectionClass(Article::class);
        $attrs = $reflector->getAttributes();
        foreach ($attrs as $attr) {
            echo $attr->getName(); echo PHP_EOL;  // attr:'ApiX\Api\Resource'
            var_dump($attr->getArguments()); echo PHP_EOL; // [0=>object(ApiX\Parameter)]
            var_Dump($attr->getTarget()); echo PHP_EOL;   // 1
            var_Dump($attr->newInstance()); echo PHP_EOL;  // attr:object ApiX\Api\Resource
        }
    }
}
