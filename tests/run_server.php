<?php

declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';

$container = new FrameworkX\Container([
    //\ApiX\Api\Specification\ApiSpecification::class => static fn() => $apixDefinition,

    \Psr\Http\Message\ResponseInterface::class => static function () {
        return \React\Http\Message\Response::class; // factory fun returns class implementing interface
    },
    'apixDebug' => true,
    'X_LISTEN' => static fn(?string $PORT = '8080') => '0.0.0.0:' . $PORT,      // built-in env var, we change it like this

    // Access logger
    FrameworkX\AccessLogHandler::class => fn () => new FrameworkX\AccessLogHandler(),

    // Error handler (here we re-set it to default)
    FrameworkX\ErrorHandler::class => static fn () => new FrameworkX\ErrorHandler(),
]);

//# create api spec, set it globally
// create app
//# register routes

//# intercept req-res, get spec from globally
//    - in Controller

$app = new FrameworkX\App(
    $container,
    FrameworkX\AccessLogHandler::class,
    FrameworkX\ErrorHandler::class,
);

\ApiX\Api\Integrator\FrameworkX\Bridge::fromPhpAttributes([
    'ApiXTests\\' => __DIR__ . '/../tests',
], $app);

//$app->get('/', new \App\Controller\Examples\HomeController());
//$app->get('/users/{name}',
//    function (Psr\Http\Message\ServerRequestInterface $request, callable $next) {
//        $response = $next($request);
//        return match (true) {
//            $response instanceof PromiseInterface => $response->then(fn (ResponseInterface $response) => $this->handle($response)),
//            $response instanceof \Generator => (fn () => $this->handle(yield from $response))(),
//            default => $this->handle($response),
//        };
//        return $response;
//    },
//    \App\Controller\Examples\MimicController::class
//);
//$app->map(['GET', 'POST'], '/user/{id}', $controller);
//$app->any('/user/{id}', $controller);
//$app->redirect('/promo/reactphp', 'https://reactphp.org/');       302 Found
//$app->redirect('/blog.html', '/blog', React\Http\Message\Response::STATUS_MOVED_PERMANENTLY);

// Route doesn't exist: 404 Not Found
// Method not registered for route: 405 Method Not Allowed

// default listen address: http://127.0.0.1:8080
// to change it: X_LISTEN=0.0.0.0:8080 php app/run.php
$app->run();
