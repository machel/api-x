<?php

declare(strict_types=1);

namespace ApiXTests\Endpoint;

use ApiX\Api\Integrator\Context;
use ApiX\Api\Integrator\FrameworkX\Controller;
use ApiXTests\Parameter\Exten;
use ApiXTests\Resource\Article;
use Psr\Http\Message\ServerRequestInterface;

#[\ApiX\Endpoint(
    '/article/{slug}/part/{part}?{exten}',
    Article::class,
)]
#[\ApiX\Parameters([
    new \ApiX\Parameter(
        'slug',
        \ApiX\In::PATH,
        \ApiX\Required::TRUE,
        new \ApiX\Type\OasString(allowed: ['json','xml'], default: 'json'),
        description: 'description of slug',
        openapiOverride: new \ApiX\OpenapiOverride([   // 'openapi' => // will override /\
            'schema' => [
                'default' => 'xml',
            ]
        ]),
    ),
    new \ApiX\Parameter(
        'part',
        \ApiX\In::PATH,
        \ApiX\Required::TRUE,
        new \ApiX\Type\OasObject([
            'tags' => new \ApiX\Type\OasArray(
                new \ApiX\Type\OasObject([
                    'id' => new \ApiX\Type\OasInteger(\ApiX\Type\IntegerFormat::INT64),
                    'tagName' => new \ApiX\Type\OasString(),
                ]),
                \ApiX\Required::TRUE,
            ),
            'color' => new \ApiX\Type\AnyOf(
                new \ApiX\Type\OasString(),
                new \ApiX\Type\OasInteger(),
            ),
            'groups' => new \ApiX\Type\OasArray(
                new \ApiX\Type\Ref('#/components/schemas/Group'),
            )
        ]),
        serializationFormat: \ApiX\SerializationFormat::APPLICATION_JSON,  // => 'content'
    ),
    new Exten('exten'),
])]
#[\ApiX\Responses([
    '200' => new \ApiX\Response(
        'successful operation',
        [\ApiX\SerializationFormat::APPLICATION_JSON, \ApiX\SerializationFormat::APPLICATION_XML]
    ),
    '404' => new \ApiX\Response(
        'article not found',
        [],
        \InvalidArgumentException::class,
    )
])]
class ArticlePartController extends Controller
{
    #[\ApiX\Get(
        'Single article',
        'Single article in JSON format',
        'getArticlePart',  // operationId
    )]
    public function get(ServerRequestInterface $request, Context $apix): Article
    {
        $slug = $apix->parameter('slug')->getAsString();

        $part = $apix->parameter('part')->getAsArray();

        /** @var Exten $exten */
        $exten = $apix->parameter('exten');
        //$id = $exten->getId();
        //$tags = $exten->getTags();

        /** @var string $slug */

        dump($slug);
        dump($part);
        dump($exten);

        //throw new \Exception('some error');  // => 500 (by default)
        //throw new \InvalidArgumentException('invalid argument');  // => 400 (configured)

        $article = new Article();

        return $article;    // => 200
    }

    #[\ApiX\Put(
        'Update existing article',
        'Update existing article by slug',
        'updateArticle',  // operationId
        [       // override /\ responses
            '200' => new \ApiX\Response(
                'successful operation',
                [\ApiX\SerializationFormat::APPLICATION_JSON]
            ),
            '404' => \ApiX\Response::DEFAULT,
        ]
    )]
    public function put(ServerRequestInterface $request, Context $apix): Article
    {
    }
}
